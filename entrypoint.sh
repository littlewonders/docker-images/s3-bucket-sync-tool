mkdir -p ~/.config/rclone/

echo "[az]" > ~/.config/rclone/rclone.conf
echo "type = azureblob" >> ~/.config/rclone/rclone.conf
echo "account = $AZURE_STORAGE_ACCOUNT" >> ~/.config/rclone/rclone.conf
echo "service_principal_file = /credentials.json" >> ~/.config/rclone/rclone.conf

echo "[amz]" >> ~/.config/rclone/rclone.conf
echo "type = s3" >> ~/.config/rclone/rclone.conf
echo "provider = AWS" >> ~/.config/rclone/rclone.conf
echo "env_auth = true" >> ~/.config/rclone/rclone.conf
echo "acl = private" >> ~/.config/rclone/rclone.conf

echo "$AZURE_FILE" > /credentials.json

rclone copy -v "amz:$SOURCE_BUCKET" "az:$DESTINATION_CONTAINER"
date | rclone rcat "az:${DESTINATION_CONTAINER}/lastrun.txt"