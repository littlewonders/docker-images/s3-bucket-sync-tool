# S3 Bucket Sync Tool

Docker image which includes RClone to allow an Amazon S3 bucket to be synced with an Azure Storage Container. The sync will not delete files in the Azure Container at all, so every file which has ever existed in the S3 bucket will exist forever in Azure.