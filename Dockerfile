FROM alpine

ARG arch="arm64"

RUN \
    apk add curl --no-cache && \
    curl -O https://downloads.rclone.org/rclone-current-linux-${arch}.zip && \
    unzip rclone-current-linux-*.zip && \
    rm -rf *.zip && \
    cd rclone-* && \
    cp rclone /usr/bin/ && \
    chown root:root /usr/bin/rclone && \
    chmod 755 /usr/bin/rclone

ADD ./entrypoint.sh /entrypoint.sh

CMD ["sh", "/entrypoint.sh"]